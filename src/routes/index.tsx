import React from 'react';

import { Switch, Route } from 'react-router-dom';

import Home from 'pages/Home';
import Details from 'pages/Details';
import EditCountry from 'pages/EditCountry';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/" exact>
      <Home />
    </Route>

    <Route path="/details/:countryName+" exact>
      <Details />
    </Route>

    <Route path="/edit/:countryName+" exact>
      <EditCountry />
    </Route>
  </Switch>
);

export default Routes;
