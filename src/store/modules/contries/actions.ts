import { gql } from '@apollo/client';
import { Dispatch } from 'redux';

import client from 'services/client';

import { Action, Country } from './types';

export const getListOfCountriesLoading = (): Action<Country[]> => {
  return {
    type: 'GET_COUNTRIES_LOADING',
    payload: {
      countries: [],
    },
  };
};

export const getListOfCountriesSuccess = (
  countries: Country[],
): Action<Country[]> => {
  return {
    type: 'GET_COUNTRIES_SUCCESS',
    payload: {
      countries,
    },
  };
};

export const getListOfCountriesFailed = (): Action<Country[]> => {
  return {
    type: 'GET_COUNTRIES_FAILED',
    payload: {
      countries: [],
    },
  };
};

export const getListOfCountries = (search = '') => {
  return async (dispatch: Dispatch): Promise<void> => {
    dispatch(getListOfCountriesLoading());

    try {
      const result = await client.query<{ Country: Country[] }>({
        query: gql`
          query($search: String) {
            Country(filter: { name_contains: $search }) {
              name
              area
              population
              capital
              flag {
                svgFile
              }
              topLevelDomains {
                name
              }
            }
          }
        `,
        variables: { search },
      });

      const countriesGraph = result.data?.Country;

      if (countriesGraph) {
        const storagedCountries = localStorage.getItem(
          '@CountryExplorer:countries',
        );

        let countries: Country[];

        if (storagedCountries) {
          const parsedCountries = JSON.parse(storagedCountries) as Country[];

          countries = countriesGraph.map(country => {
            const foundedStoragedCountry = parsedCountries.find(
              item => item.name === country.name,
            );

            return foundedStoragedCountry || country;
          });
        } else {
          countries = countriesGraph;
        }

        dispatch(getListOfCountriesSuccess(countries));
      } else dispatch(getListOfCountriesFailed());
    } catch (err) {
      dispatch(getListOfCountriesFailed());
    }
  };
};

export const saveEditedCountry = (country: Country) => {
  return async (dispatch: Dispatch): Promise<void> => {
    const storagedCountries = localStorage.getItem(
      '@CountryExplorer:countries',
    );

    const countries = storagedCountries
      ? (JSON.parse(storagedCountries) as Country[])
      : [];

    const hasEditedCountryIndex = countries.findIndex(
      item => item.name === country.name,
    );

    if (hasEditedCountryIndex >= 0) {
      countries.splice(hasEditedCountryIndex, 1, country);
    } else {
      countries.push(country);
    }

    localStorage.setItem(
      '@CountryExplorer:countries',
      JSON.stringify(countries),
    );

    try {
      const result = await client.query<{ Country: Country[] }>({
        query: gql`
          query {
            Country {
              name
              area
              population
              capital
              flag {
                svgFile
              }
              topLevelDomains {
                name
              }
            }
          }
        `,
      });

      const countriesGraph = result.data?.Country;

      if (countriesGraph) {
        const countriesResult = countriesGraph.map(countryItem => {
          const foundedStoragedCountry = countries.find(
            item => item.name === countryItem.name,
          );

          return foundedStoragedCountry || countryItem;
        });

        dispatch(getListOfCountriesSuccess(countriesResult));
      } else dispatch(getListOfCountriesFailed());
    } catch (err) {
      dispatch(getListOfCountriesFailed());
    }
  };
};
