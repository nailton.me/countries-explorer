import { CountriesState, Action, Country } from './types';

export const INITIAL_STATE: CountriesState = {
  countries: [],
  loading: false,
};

export default function contries(
  state = INITIAL_STATE,
  action: Action<Country[] | Country>,
): CountriesState {
  switch (action.type) {
    case 'GET_COUNTRIES_LOADING':
      return {
        ...state,
        loading: true,
      };
    case 'GET_COUNTRIES_SUCCESS':
      return {
        ...state,
        countries: action.payload.countries as Country[],
        loading: false,
      };

    case 'GET_COUNTRIES_FAILED':
      return {
        ...state,
        countries: [],
        loading: false,
      };

    default:
      return state;
  }
}
