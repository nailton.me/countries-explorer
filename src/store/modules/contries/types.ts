export interface Action<T> {
  type:
    | 'GET_COUNTRIES_LOADING'
    | 'GET_COUNTRIES_SUCCESS'
    | 'GET_COUNTRIES_FAILED';
  payload: Record<string, T>;
}

export interface Country {
  name: string;
  capital: string;
  area: number;
  population: number;
  flag: {
    svgFile: string;
  };
  topLevelDomains: {
    name: string;
  }[];
}

export type CountriesAction = {
  type: 'GET_LIST';
  payload: Country[];
};

export interface CountriesState {
  countries: Country[];
  loading: boolean;
}
