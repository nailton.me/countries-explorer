import { combineReducers } from 'redux';

import countries from './contries/reducer';

export default combineReducers({
  countries,
});
