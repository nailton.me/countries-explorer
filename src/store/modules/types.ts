import { CountriesState } from './contries/types';

export interface StoreState {
  countries: CountriesState;
}
