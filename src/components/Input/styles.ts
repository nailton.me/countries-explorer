import styled, { css } from 'styled-components';

interface ContainerProps {
  hasError: boolean;
}

export const Container = styled.div<ContainerProps>`
  width: 100%;

  span {
    display: block;
    font-size: 18px;
    margin-bottom: 8px;
    color: #424242;

    @media (max-width: 600px) {
      font-size: 16px;
    }
  }

  input {
    height: 64px;
    padding: 0 20px;
    width: 100%;
    border-radius: 5px;
    color: #292929;
    border: 3px solid #fff;
    font-size: 18px;

    ${props =>
      props.hasError &&
      css`
        border-color: #e57373;
      `}
  }

  p {
    display: block;
    margin-top: 4px;
    font-size: 14px;
    color: #e57373;
  }
`;
