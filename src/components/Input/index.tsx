import React, { forwardRef } from 'react';

import { Ref, InputProps } from './types';
import { Container } from './styles';

const Input = forwardRef<Ref, InputProps>(
  ({ label: labelText, name, type = 'text', error }: InputProps, ref) => {
    return (
      <Container hasError={!!error} data-testid={`input-${name}`}>
        <span>{labelText}</span>
        <input ref={ref} name={name} type={type} />
        {error && <p>{error}</p>}
      </Container>
    );
  },
);

export default Input;
