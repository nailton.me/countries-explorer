export type Ref = HTMLInputElement;

export interface InputProps {
  label: string;
  name: string;
  type?: 'text' | 'number';
  error?: string;
}
