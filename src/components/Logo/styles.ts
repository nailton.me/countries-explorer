import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;

  svg {
    margin-right: 8px;
  }

  strong {
    font-size: 24px;
    line-height: 30px;
    color: #757575;
    opacity: 0.4;
  }
`;
