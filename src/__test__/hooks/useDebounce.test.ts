import { renderHook } from '@testing-library/react-hooks';

import useDebounce from 'hooks/useDebounce';

jest.useFakeTimers();

test('should debounce a value', async () => {
  const { result } = renderHook(() => useDebounce('test', 1000));

  expect(result.current).toBe('test');
});
