import reducer, { INITIAL_STATE } from 'store/modules/contries/reducer';
import { Action, Country } from 'store/modules/contries/types';

const list: Country[] = [
  {
    name: 'Brasil',
    area: 1500000,
    population: 200000000,
    capital: 'Brasília',
    flag: {
      svgFile: 'image-url',
    },
    topLevelDomains: [{ name: '.br' }],
  },
  {
    name: 'Argentina',
    area: 1500000,
    population: 200000000,
    capital: 'Buenos Aires',
    flag: {
      svgFile: 'image-url',
    },
    topLevelDomains: [{ name: '.ar' }],
  },
];

describe('Countries Reducer', () => {
  it('should be able to return initial data', () => {
    expect(reducer(undefined, {} as Action<Country>)).toEqual({
      countries: [],
      loading: false,
    });
  });

  it('should be able to return loading list of countries', () => {
    expect(
      reducer(INITIAL_STATE, {
        type: 'GET_COUNTRIES_LOADING',
        payload: {
          countries: [],
        },
      }),
    ).toEqual({
      countries: [],
      loading: true,
    });
  });

  it('should be able to return list of countries', () => {
    expect(
      reducer(INITIAL_STATE, {
        type: 'GET_COUNTRIES_SUCCESS',
        payload: {
          countries: list,
        },
      }),
    ).toEqual({
      countries: list,
      loading: false,
    });
  });

  it('should be able to return empty list of countries on fail', () => {
    expect(
      reducer(INITIAL_STATE, {
        type: 'GET_COUNTRIES_FAILED',
        payload: {
          countries: [],
        },
      }),
    ).toEqual({
      countries: [],
      loading: false,
    });
  });
});
