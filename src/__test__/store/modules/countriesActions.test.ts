import * as actions from 'store/modules/contries/actions';
import { Country, Action } from 'store/modules/contries/types';

const list: Country[] = [
  {
    name: 'Brasil',
    area: 1500000,
    population: 200000000,
    capital: 'Brasília',
    flag: {
      svgFile: 'image-url',
    },
    topLevelDomains: [{ name: '.br' }],
  },
  {
    name: 'Argentina',
    area: 1500000,
    population: 200000000,
    capital: 'Buenos Aires',
    flag: {
      svgFile: 'image-url',
    },
    topLevelDomains: [{ name: '.ar' }],
  },
];

describe('Countries Actions', () => {
  it('should be able to get list loading', () => {
    const expectedAction: Action<Country[]> = {
      type: 'GET_COUNTRIES_LOADING',
      payload: {
        countries: [],
      },
    };

    expect(actions.getListOfCountriesLoading()).toEqual(expectedAction);
  });

  it('should be able to get list with success', () => {
    const expectedAction: Action<Country[]> = {
      type: 'GET_COUNTRIES_SUCCESS',
      payload: {
        countries: list,
      },
    };

    expect(actions.getListOfCountriesSuccess(list)).toEqual(expectedAction);
  });

  it('should be able to get list with fail', () => {
    const expectedAction: Action<Country[]> = {
      type: 'GET_COUNTRIES_FAILED',
      payload: {
        countries: [],
      },
    };

    expect(actions.getListOfCountriesFailed()).toEqual(expectedAction);
  });

  // it('should be able to get list from graphql', () => {
  //   const expectedActions: Action<Country[]>[] = [
  //     {
  //       type: 'GET_COUNTRIES_LOADING',
  //       payload: {
  //         countries: [],
  //       },
  //     },
  //     {
  //       type: 'GET_COUNTRIES_SUCCESS',
  //       payload: {
  //         countries: list,
  //       },
  //     },
  //   ];

  //   const store = mockStore({ countries: [], loading: false });

  //   return store.dispatch(actions.getListOfCountries()).then(() => {
  //     expect(store.getActions()).toEqual(expectedActions);
  //   });
  // });
});
