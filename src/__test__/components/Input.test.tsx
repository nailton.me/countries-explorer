import React from 'react';

import { render } from '@testing-library/react';

import Input from 'components/Input';

describe('Input Component', () => {
  const label = 'Test Input';
  const name = 'test';

  it('should be able to render input with required props', () => {
    const { getByTestId } = render(<Input label={label} name={name} />);

    const component = getByTestId(`input-${name}`);

    expect(component.querySelector('span')).toHaveTextContent(label);
    expect(component.querySelector('input')).toHaveValue('');
    expect(component.querySelector('p')).toBeNull();
  });

  it('should be able to show error message', () => {
    const errorMessage = 'This is a error message';
    const { getByTestId } = render(
      <Input label={label} name={name} error={errorMessage} />,
    );

    const component = getByTestId(`input-${name}`);

    expect(component.querySelector('span')).toHaveTextContent(label);
    expect(component.querySelector('input')).toHaveValue('');
    expect(component.querySelector('p')).toHaveTextContent(errorMessage);
  });
});
