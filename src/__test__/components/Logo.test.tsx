import React from 'react';

import Logo from 'components/Logo';

import { render } from '@testing-library/react';

describe('Logo', () => {
  it('should be able to show Logo', () => {
    const { getByTestId } = render(
      <>
        <Logo />
      </>,
    );

    expect(getByTestId('logo-app')).toHaveTextContent('Country explorer');
  });
});
