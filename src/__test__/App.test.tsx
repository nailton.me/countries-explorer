import React from 'react';

import { render } from '@testing-library/react';

import App from 'App';

test('should be able to render <App />', () => {
  const { getByText } = render(<App />);

  expect(getByText('Explore seus paises favoritos')).toBeTruthy();
});
