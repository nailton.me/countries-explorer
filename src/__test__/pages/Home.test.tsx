import React from 'react';

import { render, fireEvent } from '@testing-library/react';
import { useSelector } from 'react-redux';

import { MemoryRouter } from 'react-router-dom';

import Home from 'pages/Home';
import { Country } from 'store/modules/contries/types';

const list: Country[] = [
  {
    name: 'Brasil',
    area: 1500000,
    population: 200000000,
    capital: 'Brasília',
    flag: {
      svgFile: 'image-url',
    },
    topLevelDomains: [{ name: '.br' }],
  },
  {
    name: 'Argentina',
    area: 1500000,
    population: 200000000,
    capital: 'Buenos Aires',
    flag: {
      svgFile: 'image-url',
    },
    topLevelDomains: [{ name: '.ar' }],
  },
];

const mockDispatch = jest.fn();

jest.mock('react-redux', () => ({
  useDispatch: () => mockDispatch,
  useSelector: jest.fn(fn => fn()),
}));

const mockUseSelector = useSelector as jest.Mock;

describe('<Home />', () => {
  afterEach(() => {
    mockUseSelector.mockClear();
  });

  it('should be able to show message when list is empty', () => {
    mockUseSelector.mockImplementation(cb =>
      cb({
        countries: {
          countries: [],
          loading: false,
        },
      }),
    );

    const message = 'Desculpe, sua pesquisa não retornou nenhum resultado.';

    const { getByTestId } = render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>,
      {},
    );

    expect(mockDispatch).toHaveBeenCalled();
    expect(
      getByTestId(/countries-list-container/).querySelector('div'),
    ).toHaveTextContent(message);
  });

  it('should be able to show list with countries', () => {
    mockUseSelector.mockImplementation(cb =>
      cb({
        countries: {
          countries: list,
          loading: false,
        },
      }),
    );

    const { getByTestId } = render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>,
      {},
    );

    const container = getByTestId(/countries-list-container/);

    expect(mockDispatch).toHaveBeenCalled();
    expect(container.querySelectorAll('a').length).toEqual(list.length);
    expect(container.querySelector('strong')).toHaveTextContent('Brasil');
    expect(container.querySelector('p')).toHaveTextContent('Brasília');
  });

  it('should be able to show loading Skeleton', () => {
    mockUseSelector.mockImplementation(cb =>
      cb({
        countries: {
          countries: [],
          loading: true,
        },
      }),
    );

    const { getByTestId } = render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>,
      {},
    );

    expect(mockDispatch).toHaveBeenCalled();
    expect(
      getByTestId(/countries-list-container/).querySelector('div'),
    ).toHaveAttribute('class', 'css-i3h49c-SkeletonTheme');
  });

  it('should be able to submit a search by a country', () => {
    mockUseSelector.mockImplementation(cb =>
      cb({
        countries: {
          countries: list,
          loading: false,
        },
      }),
    );

    const { getByTestId, rerender } = render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>,
      {},
    );

    const form = getByTestId(/form-search/);

    const input = form.querySelector('input');
    const button = form.querySelector('button');

    expect(input).toBeInTheDocument();

    fireEvent.change(input as HTMLInputElement, {
      target: { value: 'Brasil' },
    });

    expect(input).toHaveAttribute('value', 'Brasil');
    expect(
      getByTestId(/countries-list-container/).querySelectorAll('a').length,
    ).toEqual(list.length);

    fireEvent.click(button as HTMLButtonElement);

    mockUseSelector.mockImplementation(cb =>
      cb({
        countries: {
          countries: list.filter(country => country.name === 'Brasil'),
          loading: false,
        },
      }),
    );

    rerender(
      <MemoryRouter>
        <Home />
      </MemoryRouter>,
    );

    expect(mockDispatch).toHaveBeenCalled();
    expect(
      getByTestId(/countries-list-container/).querySelectorAll('a').length,
    ).toEqual(1);
  });
});
