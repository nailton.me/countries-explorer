import styled from 'styled-components';
import { shade } from 'polished';

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  button {
    display: flex;
    align-items: center;
    padding: 4px 8px;

    border: 0;
    color: #757575;

    svg {
      margin-right: 8px;
    }
  }
`;

export const CountryInfo = styled.div`
  margin-top: 86px;
  border-radius: 8px;
  background: #fff;

  @media (max-width: 600px) {
    margin-top: 56px;
  }

  img {
    width: 100%;
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    border-bottom: 1px solid #eeeeee;
  }
`;

export const Content = styled.section`
  display: flex;
  flex-direction: column;
  padding: 0 16px 24px 26px;

  header {
    margin-top: 24px;
    display: flex;

    div {
      flex: 1;

      strong {
        font-size: 56px;
        color: #212121;

        @media (max-width: 600px) {
          font-size: 36px;
        }
      }

      p {
        font-size: 32px;
        color: #424242;

        @media (max-width: 600px) {
          font-size: 24px;
        }
      }
    }

    a {
      display: block;
      align-self: flex-start;
      display: flex;
      align-items: center;

      text-decoration: none;
      color: #fff;
      font-size: 16px;
      background: #ffa726;
      border-radius: 50%;

      padding: 16px;

      &:hover {
        background: ${shade(0.2, '#ffa726')};
      }

      @media (max-width: 600px) {
        padding: 8px;
      }
    }
  }

  ul {
    display: flex;
    list-style: none;
    margin-top: 40px;

    justify-content: space-between;

    @media (max-width: 600px) {
      flex-direction: column;
    }

    li {
      strong {
        display: block;
        font-size: 40px;
        color: #212121;

        @media (max-width: 600px) {
          font-size: 28px;
        }
      }

      p {
        color: #616161;
      }
    }
  }
`;

export const TopLevelDomain = styled.li`
  div {
    display: flex;
    align-items: center;

    strong {
      display: block;
      font-size: 40px;
      color: #212121;
    }

    span {
      margin: 0 8px;
      font-size: 32px;
      color: #616161;
    }
  }

  p {
    color: #616161;
  }
`;
