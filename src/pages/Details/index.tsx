import React, { useMemo, useEffect } from 'react';
import { useHistory, Link, useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import { FiChevronsLeft, FiEdit3 } from 'react-icons/fi';

import { StoreState } from 'store/modules/types';
import { getListOfCountries } from 'store/modules/contries/actions';

import Logo from 'components/Logo';
import { format } from 'utils/formatNumber';

import { DetailsParams } from './types';
import { Header, CountryInfo, Content, TopLevelDomain } from './styles';

const Details: React.FC = () => {
  const { countryName } = useParams<DetailsParams>();
  const { goBack } = useHistory();

  const dispatch = useDispatch();
  const { countries } = useSelector<StoreState, StoreState['countries']>(
    state => state.countries,
  );

  useEffect(() => {
    if (countries.length <= 0) dispatch(getListOfCountries());
  }, [countries, dispatch]);

  const country = useMemo(() => {
    return countries.find(countryItem => countryItem.name === countryName);
  }, [countries, countryName]);

  return (
    <>
      <Header>
        <Logo />
        <button type="button" onClick={goBack}>
          <FiChevronsLeft />
          Voltar
        </button>
      </Header>

      {country ? (
        <CountryInfo>
          {country && (
            <>
              <img src={country.flag.svgFile} alt={country.name} />

              <Content>
                <header>
                  <div>
                    <strong>{country.name}</strong>
                    <p>{country.capital}</p>
                  </div>

                  <Link to={`/edit/${country.name}`}>
                    <FiEdit3 size={24} />
                  </Link>
                </header>

                <ul>
                  <li>
                    <strong>{format(country.area)} Km²</strong>
                    <p>Aréa</p>
                  </li>

                  <li>
                    <strong>{format(country.population)}</strong>
                    <p>População</p>
                  </li>

                  <TopLevelDomain>
                    <div>
                      {country.topLevelDomains.map((domain, index) => (
                        <React.Fragment key={domain.name}>
                          <strong>{domain.name}</strong>
                          {index + 1 < country.topLevelDomains.length && (
                            <span>&</span>
                          )}
                        </React.Fragment>
                      ))}
                    </div>
                    <p>Top level Domains</p>
                  </TopLevelDomain>
                </ul>
              </Content>
            </>
          )}
        </CountryInfo>
      ) : (
        <div style={{ marginTop: 86 }}>
          <SkeletonTheme color="#F5F5F5" highlightColor="#E0E0E0">
            <Skeleton height={600} />
            <Skeleton height={300} style={{ marginTop: 24 }} />
          </SkeletonTheme>
        </div>
      )}
    </>
  );
};

export default Details;
