import React, { useMemo, useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';

import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import { FiChevronsLeft } from 'react-icons/fi';

import { StoreState } from 'store/modules/types';
import { Country } from 'store/modules/contries/types';
import {
  getListOfCountries,
  saveEditedCountry,
} from 'store/modules/contries/actions';

import Logo from 'components/Logo';
import Input from 'components/Input';

import { EditCountryParams, FormData } from './types';
import { Header, Title, LoadingWrapper, Info, Alert, Form } from './styles';

const EditCountry: React.FC = () => {
  const { register, reset, handleSubmit, errors } = useForm<FormData>({
    mode: 'onBlur',
  });

  const [message, setMessage] = useState('');

  const { countryName } = useParams<EditCountryParams>();
  const { goBack } = useHistory();

  const dispatch = useDispatch();
  const { countries } = useSelector<StoreState, StoreState['countries']>(
    state => state.countries,
  );

  useEffect(() => {
    if (countries.length <= 0) dispatch(getListOfCountries());
  }, [countries, dispatch]);

  const country = useMemo(() => {
    return countries.find(countryItem => countryItem.name === countryName);
  }, [countries, countryName]);

  useEffect(() => {
    if (country) {
      const topLevelDomain = country.topLevelDomains.reduce(
        (accumulated, current) => {
          if (!accumulated) return current.name;
          return `${accumulated}/${current.name}`;
        },
        '',
      );

      reset({
        capital: country.capital,
        area: country.area,
        population: country.population,
        topLevelDomain,
      });
    }
  }, [country, reset]);

  const onSubmit = handleSubmit(data => {
    const { capital, area, population, topLevelDomain } = data;

    if (country) {
      const countryData: Country = {
        ...country,
        capital,
        area: Number(area),
        population: Number(population),
        topLevelDomains: topLevelDomain
          .split('/')
          .map(value => ({ name: value })),
      };

      dispatch(saveEditedCountry(countryData));

      setMessage('Salvo com sucesso');

      setTimeout(() => setMessage(''), 1000);
    }
  });

  return (
    <>
      <Header>
        <Logo />
        <button type="button" onClick={goBack}>
          <FiChevronsLeft />
          Voltar
        </button>
      </Header>

      <Title>Editando</Title>

      {country ? (
        <Info>
          <img src={country.flag.svgFile} alt={country.name} />
          <strong>{country.name}</strong>
        </Info>
      ) : (
        <SkeletonTheme color="#F5F5F5" highlightColor="#E0E0E0">
          <LoadingWrapper>
            <Skeleton width={160} height={100} />
            <Skeleton
              width="100%"
              height={32}
              style={{ flex: 1, marginLeft: 16 }}
            />
          </LoadingWrapper>
        </SkeletonTheme>
      )}

      {message && <Alert>{message}</Alert>}

      <Form onSubmit={onSubmit}>
        <Input
          name="capital"
          type="text"
          label="Capital"
          ref={register({ required: 'Campo obrigatório' })}
          error={errors.capital?.message}
        />
        <Input
          name="area"
          type="number"
          label="Aréa (Km²)"
          ref={register({
            pattern: {
              value: /^[0-9]*$/,
              message: 'Deve ser um número inteiro',
            },
            required: 'Campo obrigatório',
          })}
          error={errors.area?.message}
        />
        <Input
          name="population"
          type="number"
          label="População"
          ref={register({
            pattern: {
              value: /^[0-9]*$/,
              message: 'Deve ser um número inteiro',
            },
            required: 'Campo obrigatório',
          })}
          error={errors.population?.message}
        />
        <Input
          name="topLevelDomain"
          type="text"
          label="Top Level Domain (Divida-os usando a / )"
          ref={register({ required: 'Campo obrigatório' })}
          error={errors.topLevelDomain?.message}
        />

        <button type="submit">Salvar</button>
      </Form>
    </>
  );
};

export default EditCountry;
