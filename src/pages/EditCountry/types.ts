export interface EditCountryParams {
  countryName: string;
}

export interface FormData {
  capital: string;
  area: number;
  population: number;
  topLevelDomain: string;
}
