import styled from 'styled-components';
import { shade } from 'polished';

export const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;

  button {
    display: flex;
    align-items: center;
    padding: 4px 8px;

    border: 0;
    color: #757575;

    svg {
      margin-right: 8px;
    }
  }
`;

export const Title = styled.h1`
  font-size: 48px;
  color: #212121;
  max-width: 500px;
  line-height: 56px;

  margin-top: 86px;

  @media (max-width: 600px) {
    margin-top: 56px;
  }
`;

export const LoadingWrapper = styled.div`
  margin-top: 32px;
  width: 100%;
  max-width: 700px;
  display: flex;
  align-items: center;
`;

export const Info = styled.header`
  display: flex;
  align-items: center;

  margin-top: 32px;

  @media (max-width: 600px) {
    flex-direction: column;
    align-items: flex-start;
  }

  img {
    width: 160px;
    border-radius: 4px;
  }

  strong {
    margin-left: 16px;
    font-size: 32px;
    color: #212121;

    @media (max-width: 600px) {
      margin-left: 0;
    }
  }
`;

export const Alert = styled.p`
  margin-top: 24px;
  width: 700px;
  padding: 16px 0;
  display: flex;
  justify-content: center;
  background: #81c784;
  border-radius: 8px;
  color: #fff;
  font-size: 16px;
`;

export const Form = styled.form`
  margin-top: 48px;
  max-width: 700px;
  /* display: flex; */
  /* flex-direction: column; */

  div + div {
    margin-top: 16px;
  }

  button {
    margin-top: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 64px;
    width: 100%;
    padding: 0 18px;
    background: #ffa726;
    border-radius: 5px;
    border: 0;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;

    transition: background-color 0.2s;

    &:hover {
      background: ${shade(0.2, '#FFA726')};
    }
  }
`;
