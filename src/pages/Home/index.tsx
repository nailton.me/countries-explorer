import React, { useState, FormEvent, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import { FiSearch, FiChevronRight } from 'react-icons/fi';

import { StoreState } from 'store/modules/types';
import { getListOfCountries } from 'store/modules/contries/actions';

import Logo from 'components/Logo';

import useDebounce from 'hooks/useDebounce';

import { Title, Form, Countries, Message } from './styles';

const Home: React.FC = () => {
  const [search, setSearch] = useState('');

  const dispatch = useDispatch();
  const { countries, loading } = useSelector<
    StoreState,
    StoreState['countries']
  >(state => state.countries);

  const debouncedSearch = useDebounce(search, 1000);

  const handleSearchCountry = (event: FormEvent<HTMLFormElement>): void => {
    event.preventDefault();

    dispatch(getListOfCountries(search));
  };

  useEffect(() => {
    dispatch(getListOfCountries(debouncedSearch));
  }, [debouncedSearch, dispatch]);

  return (
    <>
      <Logo />

      <Title>Explore seus paises favoritos</Title>

      <Form onSubmit={handleSearchCountry} data-testid="form-search">
        <input
          type="text"
          value={search}
          onChange={e => setSearch(e.target.value)}
          placeholder="Digite parte ou o nome completo do País"
        />

        <button type="submit">
          <FiSearch size={32} />
          <span>Pesquisar</span>
        </button>
      </Form>

      <Countries data-testid="countries-list-container">
        {loading ? (
          <SkeletonTheme color="#F5F5F5" highlightColor="#E0E0E0">
            <Skeleton
              height={100}
              width="100%"
              count={7}
              style={{ maxWidth: 700, marginBottom: 16 }}
            />
          </SkeletonTheme>
        ) : (
          <>
            {countries.length > 0 ? (
              countries.map(country => (
                <Link to={`/details/${country.name}`} key={country.name}>
                  <img src={country.flag.svgFile} alt={country.name} />
                  <div>
                    <strong>{country.name}</strong>
                    <p>{country.capital}</p>
                  </div>
                  <FiChevronRight size={24} />
                </Link>
              ))
            ) : (
              <Message>
                <span>
                  Desculpe, sua pesquisa não retornou nenhum resultado.
                </span>
              </Message>
            )}
          </>
        )}
      </Countries>
    </>
  );
};

export default Home;
