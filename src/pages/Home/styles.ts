import styled from 'styled-components';
import { shade } from 'polished';

export const Title = styled.h1`
  font-size: 48px;
  color: #212121;
  max-width: 500px;
  line-height: 56px;

  margin-top: 86px;

  @media (max-width: 600px) {
    margin-top: 56px;
  }
`;

export const Form = styled.form`
  margin-top: 32px;
  max-width: 700px;

  display: flex;

  input {
    flex: 1;
    height: 64px;
    padding: 0 20px;
    border: 0;
    border-radius: 5px 0 0 5px;
    color: #424242;
    border-right: 0pc;

    &::placeholder {
      color: #bdbdbd;
    }
  }

  button {
    display: flex;
    align-items: center;
    padding: 0 18px;
    background: #ffa726;
    border-radius: 0 5px 5px 0;
    border: 0;
    color: #fff;
    font-weight: bold;

    transition: background-color 0.2s;

    &:hover {
      background: ${shade(0.2, '#FFA726')};
    }

    span {
      margin-left: 16px;
      @media (max-width: 600px) {
        display: none;
      }
    }
  }
`;

export const Countries = styled.div`
  margin-top: 90px;
  max-width: 700px;

  @media (max-width: 600px) {
    margin-top: 60px;
  }

  a {
    background: #fff;
    border-radius: 6px;
    width: 100%;
    padding: 24px;
    display: block;
    text-decoration: none;

    display: flex;
    align-items: center;
    transition: transform 0.2s;

    & + a {
      margin-top: 16px;
    }

    &:hover {
      transform: scale(1.1);
    }

    img {
      width: 86px;
      border-radius: 4px;
      border: 1px solid #eeeeee;
    }

    div {
      margin: 0 16px;
      flex: 1;

      strong {
        font-size: 20px;
        color: #424242;
      }

      p {
        font-size: 18px;
        color: #bdbdbd;
        margin-top: 8px;
      }
    }

    svg {
      margin-left: auto;
      color: #bdbdbd;
    }
  }
`;

export const Message = styled.div`
  margin-top: 48px;
  display: flex;
  justify-content: center;

  span {
    max-width: 450px;
    font-size: 24px;
    color: #424242;
    text-align: center;
  }
`;
