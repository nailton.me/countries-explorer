export const format = (value: number): string => {
  return Intl.NumberFormat().format(value);
};
